<?php
$data = [
    'server' => [
        'items' => [
            'php' => [
                'label' => 'PHP',
                'services' => [
                    'PHP Basic',
                    'PHP Yii2',
                    'PHP Laravel',
                    'PHP Symfony'
                ],
            ],
            'python' => [
                'label' => 'Python',
                'services' => [
                    'Python Basic',
                    'Python Django',
                    'Python Flask',
                    'Python Pyramid'
                ]
            ]
        ],
        'label' => 'Server-Side'
    ],
    'client' => [
        'items' => [
            'javascript' => [
                'label' => 'JavaScript',
                'services' => [
                    'Ecmascript 6',
                    'VueJS'
                ]
            ],
            'android' => [
                'label' => 'Android',
                'services' => [
                    'Java Basic',
                    'Android SDK'
                ]
            ],
            'ios' => [
                'label' => 'iOS',
                'services' => [
                    'Swift Basic',
                    'Cocoa Framework'
                ]
            ]
        ],
        'label' => 'Client-Side'
    ]
];


$template = <<<HTM
<h1>Предлагаемые услуги:</h1>
    <ul class="nav nav-tabs" role="tablist">
        {{tab}}
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#tab-{{index}}" role="tab" aria-controls="home" aria-selected="true">{{label}}</a>
            </li>
        {{/tab}}
    </ul>
    <div class="tab-content">
        {{content}}
            <div class="tab-pane fade show active" id="tab-{{index}}" role="tabpanel" aria-labelledby="home-tab">
                {{language}}
                    <div class="card d-inline-block" style="width: 18rem;">
                        <img class="card-img-top" src="https://dummyimage.com/250/000000/ffffff" alt="{{title}}">
                        <div class="card-body">
                            <h5 class="card-title">{{title}}</h5>
                            <p class="card-text">
                            <ul class="list-group list-group-flush">
                                {{services}}
                                    <li class="list-group-item">{{service-title}}</li>
                                {{/services}}
                            </ul>
                            </p>
                            <a href="#" class="btn btn-primary">Записаться</a>
                        </div>
                    </div>
                {{/language}}
            </div>
        {{/content}}
    </div>
HTM;

$languageServer = "";
$languageClient = "";
$phpServices = "";
$phytonServices = "";
$jsServices = "";
$androidServices = "";
$iosServices = "";

preg_match_all('~({{services}})(?<services>[\s\S]+)({{\/services}})~', $template, $services);

foreach ($data['server']['items']['php']['services'] as $k) {
    $phpServices .= preg_replace('~{{service-title}}~', $k, $services['services'][0]);
}
foreach ($data['server']['items']['python']['services'] as $k) {
    $pythonServices .= preg_replace('~{{service-title}}~', $k, $services['services'][0]);
}
foreach ($data['client']['items']['javascript']['services'] as $k) {
    $jsServices .= preg_replace('~{{service-title}}~', $k, $services['services'][0]);
}
foreach ($data['client']['items']['android']['services'] as $k) {
    $androidServices .= preg_replace('~{{service-title}}~', $k, $services['services'][0]);
}
foreach ($data['client']['items']['ios']['services'] as $k) {
    $iosServices .= preg_replace('~{{service-title}}~', $k, $services['services'][0]);
}
preg_match_all('~({{language}})(?<language>[\s\S]+)({{\/language}})~', $template, $language);


//Вывод блока языков
foreach ($data['server']['items'] as $k => $v) {
    $languageServer .= preg_replace('~{{title}}~', $v['label'], $language['language'][0]);
    if ($k == 'php') {
        $languageServer = preg_replace('~{{services}}[\s\S]+{{\/services}}~', $phpServices, $languageServer);
    }
    if ($k == 'python') {
        $languageServer = preg_replace('~{{services}}[\s\S]+{{\/services}}~', $pythonServices, $languageServer);
    }
}

foreach ($data['client']['items'] as $k => $v) {
    $languageClient .= preg_replace('~{{title}}~', $v['label'], $language['language'][0]);

    if ($k == 'javascript') {
        $languageClient = preg_replace('~{{services}}[\s\S]+{{\/services}}~', $jsServices, $languageClient);
    }
    if ($k == 'android') {
        $languageClient = preg_replace('~{{services}}[\s\S]+{{\/services}}~', $androidServices, $languageClient);
    }
    if ($k == 'ios') {
        $languageClient = preg_replace('~{{services}}[\s\S]+{{\/services}}~', $iosServices, $languageClient);
    }
}


preg_match_all('~({{tab}})(?<tab>[\s\S]+)({{\/tab}})~', $template, $tab);

function tab($tab, $data)
{
    $line = "";
    $result = "";
    $index = 1;

    foreach ($data as $key) {
        $line = preg_replace('~{{label}}~', $key['label'], $tab['tab'][0]);
        $line = preg_replace('~{{index}}~', $index, $line);
        $result .= $line;
        $index++;
    }

    return $result;
}

$tab = tab($tab, $data);
$template = preg_replace('~({{tab}})(?<tab>[\s\S]+)({{\/tab}})~', $tab, $template);

preg_match_all('~({{content}})(?<content>[\s\S]+)({{\/content}})~', $template, $content);

function content($data, $content)
{
    global $languageServer;
    global $languageClient;
    $line = "";
    $lineLanguage = "";
    $result = "";
    for ($i = 1; $i <= count($data); $i++) {
        $line = preg_replace('~{{index}}~', $i, $content['content'][0]);
        $result .= $line;
        if ($i == 1) {
            $result = preg_replace('~{{language}}[\s\S]+{{\/language}}~', $languageServer, $result);
        }
        if ($i == 2) {
            $result = preg_replace('~{{language}}[\s\S]+{{\/language}}~', $languageClient, $result);
        }
    }

    return $result;
}

$content = content($data, $content);

$template = preg_replace('~{{content}}[\s\S]+{{\/content}}~', $content, $template);





